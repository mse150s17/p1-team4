#Our Project 1 starter program for calculating pi
import numpy
import matplotlib.pyplot as plt


#We want to calculate pi using the Monte Carlo Method
#Throw darts at a square dartboard

#Count darts that go in circle vs total number of darts = pi/4

#Need ability to throw darts at a square
dart_board = []

def throw_dart():
	x = numpy.random.random() #each of these should be between 0 and 1
	y = numpy.random.random()
	return x,y
	
    
#throw_darts is a function that expands on the normal throw_dart function. It lets a user choose a
#set number of darts to throw and gives an array with the x,y coordinates for all of those darts.
def throw_darts(num): #The user inputs the desired amount of thrown darts as num
	count = range(0,num) #count creates a range of integers from 0 to the desired num
	dart_board = [] #dart_board is the basic empty array that will store all x,y coordinates
	for i in count: # This for loop generates the desired x,y coordinates for each integer in count
		x = numpy.random.random() 
		y = numpy.random.random()
		dart_board.append([x,y])
	return(dart_board) #Shows all the x,y coordinates generated.
    
#Need ability to determine if the dart is in the circle
def in_circle(point): 
	d = (point[0]**2 + point[1]**2)**(0.5)
	if d > 1.0:
		return False
	return True

#pi_calc is a function that expands on the normal throw_dart and in_circle function. It lets a user choose a
#set number of darts to throw and checks if each dart is in or out a circle for all of those darts. 
#lastly it calculates pi based on the ratio of hits to the number of darts thrown.
def pi_calc(num): #The user inputs the desired amount of thrown darts as num
	count = range(0,num) #count creates a range of integers from 0 to the desired num
	misses = 0 #misses and hits allow the two values to be added to on each loop.
	hits = 0 
	dart_board_x = []
	dart_board_y = []#dart_board is the basic empty array that will store all x,y coordinates
	for i in count: # This for loop generates the desired x,y coordinates for each integer in count.
		x = (numpy.random.random() - 0.5)*2
		y = (numpy.random.random() - 0.5)*2
		dart_board_x.append(x)#dart_board is the basic empty array that will store all x,y coordinates
		dart_board_y.append(y)
		d = (x**2 + y**2)**(0.5) #this tests if the x,y coordinates are in or out of the circle and adds to hit/miss accordingly
		if d > 1.0:
			misses += 1
		else:
			hits += 1
	total = hits + misses # Used to confirm if it is the same as num input
	pi_estimate = 4 * hits/num #Uses ratio given a and number of hits to calcute pi
	print('Hits: ', hits) #Prints hits, misses, total, and our estimate of pi.
	print('Misses: ',misses)
	print('Total: ',total)
	print('Estimate of Pi: ',pi_estimate)
	
	pi = 3.1415926535897932384626433832795028841971693993751 #start of error
	error = (float(pi_estimate - pi)/float(pi))*100 #middle of error
	print("Error =", abs(error),"%") #end of error
	
	X = dart_board_x #START OF PLOT
	Y = dart_board_y
	
	h = numpy.linspace(-1.0, 1.0, 100)
	k = numpy.linspace(-1.0, 1.0, 100)
	H,K = numpy.meshgrid(h,k)
	F = H**2 + K**2
	plt.gca().set_aspect('equal')
	plt.contour(H,K,F,[1], colors='red')
	plt.scatter(X,Y)
	plt.show() #END OF PLOT
	
	return pi_estimate,dart_board_x,dart_board_y #returns the two dart_boards as a tuple that we can split for visualization

#def pi_stats_visual(x,y):
	#plt.scatter(x,y)
	#plt.show()

#I moved "error" into pi_calc...
#def error(p):
	#pi = 3.1415926535897932384626433832795028841971693993751
	#error = (float(p - pi)/float(pi))*100
	#print("Error =",error,"%")


pi_calc(1000000)
