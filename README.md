# README #

#Project 1 - Version 1.0.0 -  Calculatng Pi With The Monte Carlo Method

### What is this? ###

* Quick Summary

This project is a way to practice adding, committing, pushing, and pulling information. We are using what is known as the Monte Carlo Method, a method that utilizes "probablity", as a way to determine a value for pi. This is done by setting up a program that uses a circle inside of a square which we throw darts at. The program lets us know how many darts land inside the circle compared to outside the circle. We are able to calculate a value for pi by using the ratio between hits and the total amount of darts thrown. 


### How do I get set up? ###

* Summary of set up
	
For this program set up, a square is set up with a circle that fits inside of it as r = length/2. Then for the program we throw a large amount of darts at this square, looking at an array we can tell how many darts landed inside the circle and how many landed outside. Using this information we then calculate the ratio that gives us pi.  

* Configuration

Program run in python

* Dependencies

numpy
matplotlib.pyplot as plt

* Database configuration

In the p1-team4 directory all information on the project can be found. The program can be found in the mc.py file.

* How to run tests

To run the program in python, from bash type, python (mc.py), then hit enter.

* Deployment instructions

To deploy, you must be in the proper directory. Type cd and press enter to go to the home directory. From there, type cd pi-calc/p1-team4, and press enter to get to the proper directory.

### Contribution guidelines ###

* Writing tests

The major test concerns of this project deal with the number of darts needed for our mc.py file to generate a value for pi. Otherwise any python code written outside of the mc.py file is technically unneccessary for the completion of this project

* Code review

The files in this project may be pulled from and pushed to Bitbucket at any time. However, do refrain from chaning, breaking, or flat out erasing prexisting code without reason.

* Other guidelines

Refer to this repository's admin for further details

### Who do I talk to? ###

* Repo owner or admin

Eric Jankowski

* Other community or team contact

Find community in contributors.txt
Contacts can be found in emails.txt
